#ifndef _OPTIONS__H__
#define _OPTIONS__H__

#ifndef UART_DEBUG_ON
#define UART_DEBUG_ON 0
#endif

#ifndef OS_UART_DEBUG_ON
#define OS_UART_DEBUG_ON 1
#endif

#ifndef UART_INPUT_ON
#define UART_INPUT_ON 1
#endif


#endif //_OPTIONS__H__
