#include <common.h>
#include <main.h>
#include <options.h>
#include "usart.h"
#include <stdarg.h>

extern UART_HandleTypeDef huart1;

void negation(bool* val)
{
	print("before %d\r\n", *val);
	*val = 0x01 & (~*val);
	print("after %d\r\n", *val);
}

int _read(int file, char *data, int len)
{
	HAL_UART_Receive(&huart1, (uint8_t*) data, 1, 100);
	HAL_UART_Transmit(&huart1, (uint8_t*) data, 1, 100);
	return len;
}

int _write(int file, char *data, int len)
{
	HAL_UART_Transmit_DMA(&huart1, (uint8_t*) data, (uint16_t) len);
	return len;
}

int scan(char* data)
{
#if UART_INPUT_ON
	HAL_UART_Receive(&huart1, (uint8_t*) data, 1, 100);
	return 1;
#else
	return -1;
#endif
}

int print(const char* format, ...)
{
#if UART_DEBUG_ON
	va_list args;
	va_start(args, format);
	int len = vprintf(format, args);
	va_end(args);
	return len;
#else
	return -1;
#endif
}
