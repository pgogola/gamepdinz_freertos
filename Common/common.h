#ifndef _COMMON__H__
#define _COMMON__H__

#include <stdio.h>
#include <main.h>

typedef uint8_t bool;

#define false 0
#define true (!false)

#define LCD_X_MAX_SCREEN_COORDINATION 240
#define LCD_Y_MAX_SCREEN_COORDINATION 320
#define LCD_X_MIN_SCREEN_COORDINATION 0
#define LCD_Y_MIN_SCREEN_COORDINATION 0

typedef struct
{
	uint8_t x_;
	uint8_t y_;
} Coordinate;

void negation(bool* val);

int _read(int file, char *data, int len);
int _write(int file, char *data, int len);

int print(const char* format, ...);
int scan(char* data);





#endif //_COMMON__H__
