/*
 * cpu_load.h
 *
 *  Created on: 15.11.2018
 *      Author: Piotr
 */

#ifndef CPU_LOAD_H_
#define CPU_LOAD_H_

#include <stdint.h>

typedef struct{
	uint32_t cycle_;
	uint32_t emptyCycle_;
	uint8_t load_;
}CPULoad;

void cpu_start_idle();
void cpu_stop_idle();

/*
#define traceTASK_SWITCHED_IN()\
{\
	if(pxCurrentTCB == xTaskGetIdleTaskHandle())\
	{\
		cpu_start_idle();\
	}\
}

#define traceTASK_SWITCHED_OUT()\
{\
	if(pxCurrentTCB == xTaskGetIdleTaskHandle())\
	{\
		cpu_stop_idle();\
	}\
}
*/
#endif /* CPU_LOAD_H_ */
