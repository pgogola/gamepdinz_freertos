/*
 * game.c
 *
 *  Created on: 27.08.2018
 *      Author: Piotr
 */

#include "common.h"
#include "game.h"
#include <stdlib.h>
#include "tim.h"

extern Game* game;

Game* construct_Game(uint8_t lives, KeyboardStatus* const keyboard)
{
	Game* game = (Game*) malloc(sizeof(Game));
	if (NULL != game)
	{
		game->board_ = constructor_Board();
		game->pacman_ = constructor_Pacman(1, 1, 6, 6, LCD_COLOR_YELLOW);
		game->ghostsNumber_ = 4;
		game->ghosts_ = (Ghost**) malloc(game->ghostsNumber_ * sizeof(Ghost*));
		game->score_ = 0;
		game->level_ = 1;
		game->lives_ = lives;
		game->ghostMovementType_ = SCATTER_MOVEMENT;
		game->gameStatus_ = GAME_READY_TO_RUN;
		game->keyboard_ = keyboard;
	}
	if (NULL == game->board_ || NULL == game->ghosts_
			|| NULL == game->pacman_)
	{
		destructor_Game(game);
		return NULL;
	}

	game->ghosts_[0] = constructor_Ghost(11, 16, 6, 6, LCD_COLOR_BLUE, 1, 1, 10,
			9);
	game->ghosts_[1] = constructor_Ghost(13, 16, 6, 6, LCD_COLOR_BLUE, 28, 1,
			17, 9);
	game->ghosts_[2] = constructor_Ghost(16, 16, 6, 6, LCD_COLOR_BLUE, 1, 29,
			10, 20);
	game->ghosts_[3] = constructor_Ghost(18, 16, 6, 6, LCD_COLOR_BLUE, 29, 29,
			19, 20);

	return game;
}

void destructor_Game(Game* const game)
{
	if (NULL != game)
	{
		destructor_Board(game->board_);
		destructor_Pacman(game->pacman_);
		for (uint8_t i = 0; i < game->ghostsNumber_; i++)
		{
			destructor_Ghost(game->ghosts_[i]);
		}
		free(game);
	}
}

void initialize_Game(Game* const game)
{
	if (NULL != game)
	{
		draw_Board(game->board_);
		draw_Pacman(game->pacman_);
		for (uint8_t i = 0; i < game->ghostsNumber_; i++)
		{
			draw_Ghost(game->ghosts_[i]);
		}
		BSP_LCD_SetBackColor(LCD_COLOR_BLACK);
		BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
		const char* SCORE_TEXT = "SCORE:";
		BSP_LCD_DisplayStringAt(10, 250, (uint8_t*) SCORE_TEXT, LEFT_MODE);
		printScores(game);
		const char* LIVES_TEXT = "LIVES:";
		BSP_LCD_DisplayStringAt(10, 290, (uint8_t*) LIVES_TEXT, LEFT_MODE);
		printLifes(game);
		const char* LEVEL_TEXT = "LEVEL:";
		BSP_LCD_DisplayStringAt(10, 330, (uint8_t*) LEVEL_TEXT, LEFT_MODE);
		printLifes(game);
	}
}

bool isCollisionWithWall_Ghost(Board* const board, Ghost* const ghost)
{
	bool isCollision = (board->board_[twoDim2OneDim(ghost->x_, ghost->y_)] < 7
			&& board->board_[twoDim2OneDim(ghost->x_, ghost->y_)] > 0);

#if UART_DEBUG_ON
	if (true == isCollision)
	{
		print("Collision detected: Ghost - Wall\r\n");
	}
#endif
	return isCollision;
}

bool isCollisionWithWall_Pacman(Game* const game)
{
	bool isCollision = (game->board_->board_[twoDim2OneDim(game->pacman_->x_,
			game->pacman_->y_)] < 7
			&& game->board_->board_[twoDim2OneDim(game->pacman_->x_,
					game->pacman_->y_)] > 0);

#if UART_DEBUG_ON
	if (true == isCollision)
	{
		print("Collision detected: Pacman - Wall\r\n");
	}
#endif
	return isCollision;
}

bool isCollisionWithGhost_Pacman(Ghost* const ghost, Pacman* const pacman)
{
	bool isCollision = pacman->x_ == ghost->x_ && pacman->y_ == ghost->y_;

#if UART_DEBUG_ON
	if (true == isCollision)
	{
		print("Collision detected: Pacman - Ghost\r\n");
	}
#endif
	return isCollision;
}

bool isPointScored_Pacman(Game* const game)
{
	const uint8_t val = game->board_->board_[twoDim2OneDim(game->pacman_->x_,
			game->pacman_->y_)];
	return (FOOT_DOT == val || FOOT_BIG == val);
}

void randomMove_Ghost(Board* const board, Ghost* const ghost, uint32_t random)
{
	remove_Ghost(ghost);
	switch (random)
	{
	case 0:
		moveBy_Ghost(ghost, 0, -1);
		if (isCollisionWithWall_Ghost(board, ghost))
		{
			moveBy_Ghost(ghost, 0, 1);
		}
		break;
	case 1:
		moveBy_Ghost(ghost, 0, 1);
		if (isCollisionWithWall_Ghost(board, ghost))
		{
			moveBy_Ghost(ghost, 0, -1);
		}
		break;
	case 2:
		moveBy_Ghost(ghost, -1, 0);
		if (isCollisionWithWall_Ghost(board, ghost))
		{
			moveBy_Ghost(ghost, 1, 0);
		}
		break;
	case 3:
		moveBy_Ghost(ghost, 1, 0);
		if (isCollisionWithWall_Ghost(board, ghost))
		{
			moveBy_Ghost(ghost, -1, 0);
		}
		break;
	}
	draw_Ghost(ghost);
}

void printScores(Game* const game)
{
	BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
	char buff[10];
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat"
	sprintf(buff, "%u", game->score_);
#pragma GCC diagnostic pop
	BSP_LCD_DisplayStringAt(130, 250, (uint8_t*) buff, LEFT_MODE);
}

void printLifes(Game* const game)
{
	BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
	char buff[10];
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat"
	sprintf(buff, "%u", game->lives_);
#pragma GCC diagnostic pop
	BSP_LCD_DisplayStringAt(130, 290, (uint8_t*) buff, LEFT_MODE);
}

void printLevel(Game* const game)
{
	BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
	char buff[10];
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat"
	sprintf(buff, "%u", game->level_);
#pragma GCC diagnostic pop
	BSP_LCD_DisplayStringAt(130, 330, (uint8_t*) buff, LEFT_MODE);
}

void gameInformation(const char* informationText, uint8_t line)
{
	BSP_LCD_Clear(LCD_COLOR_BLACK);
	BSP_LCD_SetTextColor(LCD_COLOR_GREEN);
	BSP_LCD_SetBackColor(LCD_COLOR_BLACK);
	BSP_LCD_DisplayStringAt(0, line * 20, (uint8_t*) informationText,
			CENTER_MODE);
}

void gameInformationLines(const char** informationText, uint8_t lines)
{
	BSP_LCD_Clear(LCD_COLOR_BLACK);
	BSP_LCD_SetTextColor(LCD_COLOR_GREEN);
	BSP_LCD_SetBackColor(LCD_COLOR_BLACK);
	for (uint8_t line = 0; line < lines; line++)
	{
		BSP_LCD_DisplayStringAt(0, line * 30 + 20,
				(uint8_t*) informationText[line], CENTER_MODE);
	}
}






