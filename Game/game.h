/*
 * game.h
 *
 *  Created on: 27.08.2018
 *      Author: Piotr
 */

#ifndef GAME_H_
#define GAME_H_

#include <lcd.h>
#include <keyboard.h>

#define SPEED_1 200
#define SPEED_2 180
#define SPEED_3 160
#define SPEED_4 140
#define SPEED_5 120
#define SPEED_6 100
#define SPEED_7 80
#define SPEED_8 60
#define SPEED_9 40

#define GHOSTS_SPEED SPEED_6


#define KEYBOARD_REFRESH_TIME_MENU 100
#define KEYBOARD_REFRESH_TIME_GAME 100



enum GhostMovementType
{
	CHASE_MOVEMENT,
	SCATTER_MOVEMENT,
	FRIGHTENED_MOVEMENT
};

enum GameStatus
{
	GAME_READY_TO_RUN,
	GAME_RUNNING,
	GAME_FINISHED,
	GAME_ERROR,
	GAME_WON,
	GAME_LOST
};

typedef struct
{
	Pacman* pacman_;
	Ghost** ghosts_;
	Board* board_;
	uint8_t lives_;
	uint32_t score_;
	uint32_t level_;
	uint8_t ghostsNumber_;

	volatile enum GhostMovementType ghostMovementType_;
	volatile enum GameStatus gameStatus_;
	KeyboardStatus* keyboard_;
} Game;

Game* construct_Game(uint8_t lives, KeyboardStatus* const keyboard);

void destructor_Game(Game* const game);

void initialize_Game(Game* const game);

bool isCollisionWithWall_Ghost(Board* const board, Ghost* const ghost);

bool isCollisionWithWall_Pacman(Game* const game);

bool isCollisionWithGhost_Pacman(Ghost* const ghost, Pacman* const pacman);

bool isPointScored_Pacman(Game* const game);

void randomMove_Ghost(Board* const board, Ghost* const ghost, uint32_t random);

void printScores(Game* const game);

void printLifes(Game* const game);

void printLevel(Game* const game);

void gameInformation(const char* informationText, uint8_t line);

void gameInformationLines(const char** informationText, uint8_t lines);

#endif /* GAME_H_ */
