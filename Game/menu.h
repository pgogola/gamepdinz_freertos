/*
 * menu.h
 *
 *  Created on: 06.10.2018
 *      Author: Piotr
 */

#ifndef MENU_H_
#define MENU_H_

#include "lcd.h"
#include "keyboard.h"

typedef struct
{
	char text_[10];
	uint32_t position_;
	bool isActive_;
} Label;

typedef struct
{
	uint8_t labelsAmount_;
	uint8_t selectedLabel_;
	Label* labels_;
	KeyboardStatus* keyboard_;
} Menu;

Menu* construct_Menu(uint8_t labelsAmount, const char** labels, KeyboardStatus* const keyboard);

void destructor_Menu(Menu* const menu);

uint8_t run_Menu(Menu* const menu);




#endif /* MENU_H_ */
