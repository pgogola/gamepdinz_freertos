/*
 * score.c
 *
 *  Created on: 27.08.2018
 *      Author: Piotr
 */


#include "score.h"

#include <stdlib.h>

Score *constructor_Score(uint32_t x, uint32_t y, uint32_t radius, uint8_t scores, uint32_t color)
{
	Score* score = (Score*) malloc(sizeof(Score));
	score->scores_ = scores;
	score->pack = constructor_Circle(x, y, radius, true,
			color);
	return score;
}

void destructor_Score(Score* const score)
{
	destructor_Circle(score->pack);
	free(score);
}

bool draw_Score(Score* const score)
{
	return draw_Circle(score->pack);
}

bool remove_Score(Score* const score)
{
	remove_Circle(score->pack);
	return true;
}

uint8_t getScores(Score* const score)
{
	return score->scores_;
}
