/*
 * point.h
 *
 *  Created on: 27.08.2018
 *      Author: Piotr
 */

#ifndef SHAPES_SCORE_H_
#define SHAPES_SCORE_H_

#include <common.h>
#include <stdint.h>
#include "circle.h"

typedef struct
{
	uint32_t color_;
	uint32_t x_;
	uint32_t y_;
	uint32_t radius_;

	uint8_t scores_;
	Circle* pack;
} Score;

Score *constructor_Score(uint32_t x, uint32_t y, uint32_t radius, uint8_t scores, uint32_t color);
void destructor_Score(Score* const score);
bool draw_Score(Score* const score);
bool remove_Score(Score* const score);
uint8_t getScores(Score* const score);


#endif /* SHAPES_SCORE_H_ */
