/*
 * pacman.c
 *
 *  Created on: 16.08.2018
 *      Author: Piotr
 */

#include <lcd.h>
#include <stdlib.h>
#include <Shapes/pacman.h>

Pacman *constructor_Pacman(uint32_t x, uint32_t y, uint32_t xSize,
		uint32_t ySize, uint32_t color)
{
	Pacman* pacman = (Pacman*) malloc(sizeof(Pacman));
	pacman->pack = constructor_Circle(x, y, xSize, true,
	color);

	pacman->x_ = x;
	pacman->y_ = y;
	pacman->lastX_ = 0;
	pacman->lastY_ = 0;
	pacman->xSize_ = xSize;
	pacman->ySize_ = ySize;
	pacman->reachedTarget_ = true;

	return pacman;
}

void destructor_Pacman(Pacman* const pacman)
{
	destructor_Circle(pacman->pack);
	free(pacman);
}

bool moveTo_Pacman(Pacman* const pacman, uint32_t x, uint32_t y)
{
	if (moveTo_Circle(pacman->pack, x, y))
	{
		pacman->x_ = x;
		pacman->y_ = y;
		return true;
	}
	return false;
}

bool moveBy_Pacman(Pacman* const pacman, int32_t x, int32_t y)
{
	if (moveBy_Circle(pacman->pack, x, y))
	{
		pacman->x_ += x;
		pacman->y_ += y;
		return true;
	}
	return false;

}

bool draw_Pacman(Pacman* const pacman)
{
	return draw_Circle(pacman->pack);
}

bool remove_Pacman(Pacman* const pacman)
{
	remove_Circle(pacman->pack);
	return true;
}

void mouthOpen_Pacman(Pacman* const pacman, bool open)
{

}



