/*
 * ghost.c
 *
 *  Created on: 16.08.2018
 *      Author: Piotr
 */

#include "ghost.h"

#include <lcd.h>
#include <stdlib.h>
#include <math.h>

float distance(int32_t currX, int32_t currY, int32_t destX, int32_t destY)
{
	float xDistance = currX - destX;
	float yDistance = currY - destY;
	float distance = 0;
	distance = sqrtf(xDistance * xDistance + yDistance * yDistance);
	return distance;
}

static void algorithm(Ghost* const ghost, uint8_t destX, uint8_t destY,
		Board* const board)
{
	float minDistance = 90;
	uint8_t dx = ghost->lastX_;
	uint8_t dy = ghost->lastY_;
	float newDistance;
	for (uint8_t x = 0; x < 3; x++)
	{
		for (uint8_t y = 0; y < 3; y++)
		{
			newDistance = distance(ghost->x_ - 1 + x, ghost->y_ - 1 + y, destX,
					destY);
			if ((x == 0 && y == 0) || (x == 2 && y == 0) || (x == 0 && y == 2)
					|| (x == 2 && y == 2) || ((x == 1 && y == 1)))
			{
				continue;
			}
			if (newDistance < minDistance
					&& (!isWall(board, ghost->x_ - 1 + x, ghost->y_ - 1 + y))
					&& (ghost->lastX_ != ghost->x_ - 1 + x
							|| ghost->lastY_ != ghost->y_ - 1 + y))
			{
				minDistance = newDistance;
				dx = ghost->x_ - 1 + x;
				dy = ghost->y_ - 1 + y;
			}
		}
	}
	ghost->lastX_ = ghost->x_;
	ghost->lastY_ = ghost->y_;
	moveTo_Ghost(ghost, dx, dy);
	if (NULL
			!= board->boardScores_[twoDim2OneDim(ghost->lastX_, ghost->lastY_)])
	{
		draw_Score(
				board->boardScores_[twoDim2OneDim(ghost->lastX_, ghost->lastY_)]);
	}
}

void chaseMovementAlgorithm(Ghost* const ghost, Pacman* const pacman,
		Board* const board)
{
	algorithm(ghost, pacman->x_, pacman->y_, board);
}

void scatterMovementAlgorithm(Ghost* const ghost, Board* const board)
{
	if (true == ghost->reachedTarget_)
	{
		ghost->targetX_ = ghost->targetPoints_[ghost->currentTargetPoint_].x_;
		ghost->targetY_ = ghost->targetPoints_[ghost->currentTargetPoint_].y_;
		ghost->currentTargetPoint_ = (ghost->currentTargetPoint_+1)%2;
		ghost->reachedTarget_ = false;
	}
	algorithm(ghost, ghost->targetX_, ghost->targetY_, board);
	if (ghost->x_ == ghost->targetX_ && ghost->y_ == ghost->targetY_)
	{
		ghost->reachedTarget_ = true;
	}
}

Ghost *constructor_Ghost(int32_t x, int32_t y, int32_t xSize, int32_t ySize,
		uint32_t color, uint8_t target1X, uint8_t target1Y, uint8_t target2X,
		uint8_t target2Y)
{
	Ghost* ghost = (Ghost*) malloc(sizeof(Ghost));
	ghost->pack_ = constructor_Circle(x, y, xSize, true, color);
	ghost->initialX_ = ghost->x_ = x;
	ghost->initialY_ = ghost->y_ = y;
	ghost->lastX_ = 0;
	ghost->lastY_ = 0;
	ghost->xSize_ = xSize;
	ghost->ySize_ = ySize;
	ghost->reachedTarget_ = true;

	ghost->targetPoints_[0].x_ = target1X;
	ghost->targetPoints_[0].y_ = target1Y;
	ghost->targetPoints_[1].x_ = target2X;
	ghost->targetPoints_[1].y_ = target2Y;
	ghost->currentTargetPoint_ = 0;
	return ghost;
}

void destructor_Ghost(Ghost* const ghost)
{
	destructor_Circle(ghost->pack_);
	free(ghost);
}

bool moveTo_Ghost(Ghost* const ghost, int32_t x, int32_t y)
{
	if (moveTo_Circle(ghost->pack_, x, y))
	{
		ghost->x_ = x;
		ghost->y_ = y;
		return true;
	}
	return false;
}

bool moveBy_Ghost(Ghost* const ghost, int32_t x, int32_t y)
{
	if (moveBy_Circle(ghost->pack_, x, y))
	{
		ghost->x_ += x;
		ghost->y_ += y;
		return true;
	}
	return false;
}

bool draw_Ghost(Ghost* const ghost)
{
	return draw_Circle(ghost->pack_);
}

bool remove_Ghost(Ghost* const ghost)
{
	remove_Circle(ghost->pack_);
	return true;
}

void resetPosition_Ghost(Ghost* const ghost)
{
	ghost->x_ = ghost->initialX_;
	ghost->y_ = ghost->initialY_;
}
