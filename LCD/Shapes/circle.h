/*
 * circle.h
 *
 *  Created on: 14.08.2018
 *      Author: Piotr
 */

#ifndef SHAPES_CIRCLE_H_
#define SHAPES_CIRCLE_H_

#include <common.h>
#include <stdint.h>

typedef struct {
	uint32_t color_;
	bool isFilledUp_;
	int32_t x_;
	int32_t y_;
	uint32_t radius_;
} Circle;

Circle *constructor_Circle(uint32_t x, uint32_t y, uint32_t radius, bool isFilledUp, uint32_t color);
void destructor_Circle(Circle* const circle);
bool moveTo_Circle(Circle* const circle, int32_t x, int32_t y);
bool moveBy_Circle(Circle* const circle, int32_t x, int32_t y);
bool draw_Circle(Circle* const circle);
bool remove_Circle(Circle* const circle);
void printInformation_Circle(Circle* const circle, const char* description);

#endif /* SHAPES_CIRCLE_H_ */
