/*
 * pacman.h
 *
 *  Created on: 16.08.2018
 *      Author: Piotr
 */

#ifndef SHAPES_PACMAN_H_
#define SHAPES_PACMAN_H_

#include <common.h>
#include "board.h"
#include "circle.h"

typedef enum
{
	UP, DOWN, RIGHT, LEFT
} Orientation;

typedef struct
{
	uint32_t color_;
	uint32_t x_;
	uint32_t y_;
	uint32_t xSize_;
	uint32_t ySize_;
	Orientation orientation;
	bool mouthOpen;
	Circle* pack;

//To test case
	bool reachedTarget_;
	uint8_t lastX_;
	uint8_t lastY_;
} Pacman;

Pacman *constructor_Pacman(uint32_t x, uint32_t y, uint32_t xSize,
		uint32_t ySize, uint32_t color);
void destructor_Pacman(Pacman* const pacman);
bool moveTo_Pacman(Pacman* const pacman, uint32_t x, uint32_t y);
bool moveBy_Pacman(Pacman* const pacman, int32_t x, int32_t y);
bool draw_Pacman(Pacman* const pacman);
bool remove_Pacman(Pacman* const pacman);
void mouthOpen_Pacman(Pacman* const pacman, bool open);

#endif /* SHAPES_PACMAN_H_ */
