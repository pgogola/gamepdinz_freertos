/*
 * rectangle.h
 *
 *  Created on: 23.08.2018
 *      Author: Piotr
 */

#ifndef SHAPES_RECTANGLE_H_
#define SHAPES_RECTANGLE_H_

#include <common.h>
#include <stdint.h>

typedef struct {
	uint32_t color_;
	bool isFilledUp_;
	int32_t xUpperLeftCorner_;
	int32_t yUpperLeftCorner_;
	int32_t xBottomRightCorner_;
	int32_t yBottomRightCorner_;
	int32_t width_;
	int32_t height_;
} Rectangle;

Rectangle *constructor_Rectangle(int32_t xUpperLeftCorner,
		int32_t yUpperLeftCorner, int32_t xBottomRightCorner,
		int32_t yBottomRightCorner, bool isFilledUp, uint32_t color);
void destructor_Rectangle(Rectangle* const rectangle);
bool moveTo_Rectangle(Rectangle* const rectangle, uint32_t xUpperLeftCorner,
		uint32_t yUpperLeftCorner);
bool moveBy_Rectangle(Rectangle* const rectangle, int32_t x, int32_t y);
bool draw_Rectangle(Rectangle* const rectangle);
void printInformation_Rectangle(Rectangle* const rectangle, const char* description);

#endif /* SHAPES_RECTANGLE_H_ */
