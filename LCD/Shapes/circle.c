/*
 * circle.c
 *
 *  Created on: 14.08.2018
 *      Author: Piotr
 */

#include "circle.h"

#include <lcd.h>
#include "stdlib.h"
#include <FreeRTOS.h>
#include <semphr.h>

extern SemaphoreHandle_t lcdMutex;
Circle *constructor_Circle(uint32_t x, uint32_t y, uint32_t radius,
		bool isFilledUp, uint32_t color)
{
	if (x < LCD_X_MIN_SCREEN_COORDINATION || y < LCD_Y_MIN_SCREEN_COORDINATION
			|| x > LCD_X_MAX_SCREEN_COORDINATION
			|| y > LCD_Y_MAX_SCREEN_COORDINATION)
	{
		return NULL;
	}
	Circle* circle = (Circle*) malloc(sizeof(Circle));
	if (NULL != circle)
	{
		circle->x_ = x;
		circle->y_ = y;
		circle->radius_ = radius;
		circle->color_ = color;
		circle->isFilledUp_ = isFilledUp;
	}
	return circle;
}

void destructor_Circle(Circle* const circle)
{
	if (NULL != circle)
	{
		free(circle);
	}
}

bool moveTo_Circle(Circle* const circle, int32_t x, int32_t y)
{
	if (NULL == circle||
	x < LCD_X_MIN_SCREEN_COORDINATION ||
	y < LCD_Y_MIN_SCREEN_COORDINATION ||
	x > LCD_X_MAX_SCREEN_COORDINATION ||
	y > LCD_Y_MAX_SCREEN_COORDINATION)
	{
		return false;
	}
	circle->x_ = x;
	circle->y_ = y;
	printInformation_Circle(circle, "move");
	return true;
}

bool moveBy_Circle(Circle* const circle, int32_t x, int32_t y)
{
	return moveTo_Circle(circle, x + circle->x_, y + circle->y_);
}

bool draw_Circle(Circle* const circle)
{
	if (NULL == circle)
	{
		return false;
	}
	if (pdPASS == xSemaphoreTake(lcdMutex, portMAX_DELAY))
	{
		printInformation_Circle(circle, "draw");
		BSP_LCD_SetTextColor(circle->color_);
		if (true == circle->isFilledUp_)
		{
			BSP_LCD_FillCircle(circle->x_ * BOARD_RATIO_X + BOARD_RATIO_X / 2,
					circle->y_ * BOARD_RATIO_Y + BOARD_RATIO_Y / 2,
					circle->radius_);
		} else
		{
			BSP_LCD_DrawCircle(circle->x_, circle->y_, circle->radius_);
		}
		xSemaphoreGive(lcdMutex);
	}
	return true;
}

bool remove_Circle(Circle* const circle)
{
	if (NULL == circle)
	{
		return false;
	}
	if (pdPASS == xSemaphoreTake(lcdMutex, portMAX_DELAY))
	{
		BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
		if (true == circle->isFilledUp_)
		{
			BSP_LCD_FillCircle(circle->x_ * BOARD_RATIO_X + BOARD_RATIO_X / 2,
					circle->y_ * BOARD_RATIO_Y + BOARD_RATIO_Y / 2,
					circle->radius_);
		} else
		{
			BSP_LCD_DrawCircle(circle->x_, circle->y_, circle->radius_);
		}
		xSemaphoreGive(lcdMutex);
	}
	return true;
}

void printInformation_Circle(Circle* const circle, const char* description)
{
	print("%s", description);
	print(" CIRCLE: x=%d\ty=%d\tradius=%d\r\n", circle->x_, circle->y_,
			circle->radius_);
}

