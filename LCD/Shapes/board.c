/*
 * board.c
 *
 *  Created on: 14.08.2018
 *      Author: Piotr
 */

#include "board.h"

#include <lcd.h>
#include "stdlib.h"

extern uint8_t boardXY[];
extern uint8_t boardScoresXY[];

static void createScoresForBoard(Board* const board)
{
	board->amountOfScores_ = 0;
	for (uint32_t x = 0; x < BOARD_SIZE_X; x++)
	{
		for (uint32_t y = 0; y < BOARD_SIZE_Y; y++)
		{
			uint32_t coordinates = twoDim2OneDim(x, y);
			if (!(isWall(board, x, y) || NO_SYMBOL == board->board_[coordinates]))
			{
				board->boardScores_[coordinates] = constructor_Score(x, y, 1, 1,
						LCD_COLOR_ORANGE);
				board->amountOfScores_++;
			} else
			{
				board->boardScores_[coordinates] = NULL;
			}
		}
	}
}

static void makeBoardTargetPoints(Board* const board)
{
	board->coordinatesAmount_ = 0;
	for (uint32_t x = 0; x < BOARD_SIZE_X; x++)
	{
		for (uint32_t y = 0; y < BOARD_SIZE_Y; y++)
		{
			if (TARGET_POINT == board->board_[twoDim2OneDim(x, y)])
			{
				board->coordinatesAmount_++;
			}
		}
	}
	board->coordinates_ = (Coordinate*) malloc(
			board->coordinatesAmount_ * sizeof(Coordinate));
	uint8_t i = 0;
	for (uint32_t x = 0; x < BOARD_SIZE_X; x++)
	{
		for (uint32_t y = 0; y < BOARD_SIZE_Y; y++)
		{
			if (TARGET_POINT == board->board_[twoDim2OneDim(x, y)])
			{
				board->coordinates_[i].x_ = x;
				board->coordinates_[i].y_ = y;
				i++;
			}
		}
	}
}

static void drawLeftUpperCorner(uint16_t x, uint16_t y, uint32_t color)
{
	BSP_LCD_SetTextColor(color);
	BSP_LCD_DrawVLine(x * BOARD_RATIO_X + BOARD_RATIO_X / 2,
			y * BOARD_RATIO_Y + BOARD_RATIO_Y / 2, BOARD_RATIO_Y / 2);
	BSP_LCD_DrawHLine(x * BOARD_RATIO_X + BOARD_RATIO_X / 2,
			y * BOARD_RATIO_Y + BOARD_RATIO_Y / 2, BOARD_RATIO_X / 2);
}

static void drawRightUpperCorner(uint16_t x, uint16_t y, uint32_t color)
{
	BSP_LCD_SetTextColor(color);
	BSP_LCD_DrawVLine(x * BOARD_RATIO_X + BOARD_RATIO_X / 2,
			y * BOARD_RATIO_Y + BOARD_RATIO_Y / 2, BOARD_RATIO_Y / 2);
	BSP_LCD_DrawHLine(x * BOARD_RATIO_X, y * BOARD_RATIO_Y + BOARD_RATIO_Y / 2,
	BOARD_RATIO_X / 2);
}

static void drawLeftBottomCorner(uint16_t x, uint16_t y, uint32_t color)
{
	BSP_LCD_SetTextColor(color);
	BSP_LCD_DrawVLine(x * BOARD_RATIO_X + BOARD_RATIO_X / 2, y * BOARD_RATIO_Y,
	BOARD_RATIO_Y / 2);
	BSP_LCD_DrawHLine(x * BOARD_RATIO_X + BOARD_RATIO_X / 2,
			y * BOARD_RATIO_Y + BOARD_RATIO_Y / 2, BOARD_RATIO_X / 2);
}

static void drawRightBottomCorner(uint16_t x, uint16_t y, uint32_t color)
{
	BSP_LCD_SetTextColor(color);
	BSP_LCD_DrawVLine(x * BOARD_RATIO_X + BOARD_RATIO_X / 2, y * BOARD_RATIO_Y,
	BOARD_RATIO_Y / 2);
	BSP_LCD_DrawHLine(x * BOARD_RATIO_X, y * BOARD_RATIO_Y + BOARD_RATIO_Y / 2,
	BOARD_RATIO_X / 2);
}

static void drawHorizontalLine(uint16_t x, uint16_t y, uint32_t color)
{
	BSP_LCD_SetTextColor(color);
	BSP_LCD_DrawHLine(x * BOARD_RATIO_X, y * BOARD_RATIO_Y + BOARD_RATIO_Y / 2,
	BOARD_RATIO_X);
}

static void drawVerticalLine(uint16_t x, uint16_t y, uint32_t color)
{
	BSP_LCD_SetTextColor(color);
	BSP_LCD_DrawVLine(x * BOARD_RATIO_X + BOARD_RATIO_X / 2, y * BOARD_RATIO_Y,
	BOARD_RATIO_Y);
}

uint32_t twoDim2OneDim(uint8_t x, uint8_t y)
{
	print("twoDim2OneDim input x=%d\ty=%d\toutput=%d\r\n", x, y,
			y * BOARD_SIZE_X + x);
	return y * BOARD_SIZE_X + x;
}

Board *constructor_Board()
{
	Board* board = (Board*) malloc(sizeof(Board));
	if (NULL != board)
	{
		board->board_ = boardXY;
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wincompatible-pointer-types"
		board->boardScores_ = (Score*) &boardScoresXY[0];
#pragma GCC diagnostic pop
		board->color_ = LCD_COLOR_GREEN;
		makeBoardTargetPoints(board);
		createScoresForBoard(board);
	}
	return board;
}

void destructor_Board(Board* const board)
{
	if (NULL != board)
	{
		for (uint8_t x = 0; x < BOARD_SIZE_X; x++)
		{
			for (uint8_t y = 0; y < BOARD_SIZE_Y; y++)
			{
				uint32_t index = twoDim2OneDim(x, y);
				if (NULL != board->boardScores_[index])
				{
					destructor_Score(board->boardScores_[index]);
				}
				board->boardScores_[index] = NULL;
			}
		}
		free(board->coordinates_);
		free(board);
	}
}

bool draw_Board(Board* const board)
{
	if (NULL == board)
	{
		return false;
	}
	BSP_LCD_Clear(LCD_COLOR_BLACK);
	BSP_LCD_SetTextColor(LCD_COLOR_GREEN);
	for (uint32_t x = 0; x < BOARD_SIZE_X; x++)
	{
		for (uint32_t y = 0; y < BOARD_SIZE_Y; y++)
		{
			switch (board->board_[twoDim2OneDim(x, y)])
			{
			case WALL_H:
				drawHorizontalLine(x, y, board->color_);
				break;
			case WALL_V:
				drawVerticalLine(x, y, board->color_);
				break;
			case WALL_LEFT_UP_CORNER:
				drawLeftUpperCorner(x, y, board->color_);
				break;
			case WALL_RIGHT_UP_CORNER:
				drawRightUpperCorner(x, y, board->color_);
				break;
			case WALL_LEFT_BOTTOM_CORNER:
				drawLeftBottomCorner(x, y, board->color_);
				break;
			case WALL_RIGHT_BOTTOM_CORNER:
				drawRightBottomCorner(x, y, board->color_);
				break;
			}
			if (NULL != board->boardScores_[twoDim2OneDim(x, y)])
			{
				draw_Score(board->boardScores_[twoDim2OneDim(x, y)]);
			}
		}
	}
	return true;
}

bool isWall(Board* const board, uint8_t x, uint8_t y)
{
	uint8_t symbol = board->board_[twoDim2OneDim(x, y)];
	return symbol < 7 && symbol > 0;
}

