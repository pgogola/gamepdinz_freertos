/*
 * others.c
 *
 *  Created on: 15.08.2018
 *      Author: Piotr
 */


typedef struct {
	void (*draw)();
//	A* (*construct)();
} A;

typedef struct {
	A x;
} B;

typedef struct {
	A x;
} C;

void drawA() {
	print("A\r\n");
}

void drawB() {
	print("B\r\n");
}

void drawC() {
	print("C\r\n");
}

A* constructA() {
	A* a = malloc(sizeof(A));
	a->draw = drawA;
	return a;
}

A* constructB() {
	B* b = malloc(sizeof(B));
	b->x.draw = drawB;
	return (A*) b;
}

A* constructC() {
	C* c = malloc(sizeof(C));
	c->x.draw = drawC;
	return (A*)c;
}

//A* aa = constructA();
//aa->draw();
//A* bb = constructB();
//bb->draw();
//A* cc = constructC();
//cc->draw();
