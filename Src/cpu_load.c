/*
 * cpu_load.c
 *
 *  Created on: 15.11.2018
 *      Author: Piotr
 */

#include "cpu_load.h"
#include "stm32f4xx_hal.h"
#include "common.h"
#include "FreeRTOS.h"
#include "task.h"

extern TIM_HandleTypeDef htim2;
extern TaskHandle_t ghostsTaskHandle;
extern TaskHandle_t pacmanTaskHandle;

void CPULoad_init(CPULoad * const cpuLoad)
{
	cpuLoad->cycle_ = 0;
	cpuLoad->emptyCycle_ = 0;
	cpuLoad->load_ = 0;
	CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
	DWT->CTRL &= ~0x00000001;
	DWT->CTRL |= 0x00000001;
	DWT->CYCCNT = 0;
}

/* USER DEFINED MACROS */
#define _ARRAY__ 0
#include "cpu_load.h"
static CPULoad cpuLoad;
static uint32_t stop = 0;
static uint32_t measureNum = 0;
#if _ARRAY__
static uint32_t tim2_autoreloadValue[] =
{	899999, 899999, 854999, 809998, 764999, 719999, 674999, 629999, 584999,
	539999, 494999, 449999, 404998, 359999, 314999, 269999, 224999, 179999,
	134999, 89999, 85499, 80999, 76499, 71999, 67499, 62999, 58499, 53999,
	49499, 44999, 40499, 35999, 31499, 26999, 22499, 17999, 13499, 8999,
	4499};
#else
static uint32_t tim2_autoreloadValue = 24999;
#endif
static volatile uint32_t index_autoreloadValue = 0;
void cpu_start_idle()
{
	cpuLoad.cycle_ += DWT->CYCCNT;
	DWT->CYCCNT = 0;
}

void cpu_stop_idle()
{
	stop = DWT->CYCCNT;
	DWT->CYCCNT = 0;
	cpuLoad.cycle_ += stop;
	cpuLoad.emptyCycle_ += stop;
#if _ARRAY__
	if (cpuLoad.cycle_ >= HAL_RCC_GetHCLKFreq() && index_autoreloadValue < 39)
#else
	if (cpuLoad.cycle_ >= HAL_RCC_GetHCLKFreq())
#endif
	{
		measureNum++;
#if _ARRAY__
		printf("%lu %lu %lu\r\n", cpuLoad.emptyCycle_, cpuLoad.cycle_,
				tim2_autoreloadValue[index_autoreloadValue]);
#else
		printf("%lu %lu %lu\r\n", cpuLoad.emptyCycle_, cpuLoad.cycle_,
				tim2_autoreloadValue);
#endif
		cpuLoad.emptyCycle_ = 0;
		cpuLoad.cycle_ = 0;
		if (measureNum >= 10)
		{
#if _ARRAY__
			index_autoreloadValue++;
			__HAL_TIM_SET_AUTORELOAD(&htim2,
					tim2_autoreloadValue[index_autoreloadValue]);
			__HAL_TIM_SET_PRESCALER(&htim2, 0);
			__HAL_TIM_SET_COUNTER(&htim2, 0);
#else
			tim2_autoreloadValue -= 5000;
			__HAL_TIM_SET_AUTORELOAD(&htim2, tim2_autoreloadValue);
			__HAL_TIM_SET_PRESCALER(&htim2, 0);
			__HAL_TIM_SET_COUNTER(&htim2, 0);
#endif
			measureNum = 0;
		}
	}
}

