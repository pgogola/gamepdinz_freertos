/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * This notice applies to any and all portions of this file
 * that are not between comment pairs USER CODE BEGIN and
 * USER CODE END. Other portions of this file, whether
 * inserted by the user or by software development tools
 * are owned by their respective copyright owners.
 *
 * Copyright (c) 2018 STMicroelectronics International N.V.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted, provided that the following conditions are met:
 *
 * 1. Redistribution of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of STMicroelectronics nor the names of other
 *    contributors to this software may be used to endorse or promote products
 *    derived from this software without specific written permission.
 * 4. This software, including modifications and/or derivative works of this
 *    software, must execute solely and exclusively on microcontroller or
 *    microprocessor devices manufactured by or for STMicroelectronics.
 * 5. Redistribution and use of this software other than as permitted under
 *    this license is void and will automatically terminate your rights under
 *    this license.
 *
 * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
 * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT
 * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_hal.h"
#include "cmsis_os.h"
#include "dma.h"
#include "dma2d.h"
#include "i2c.h"
#include "ltdc.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"
#include "fmc.h"

/* USER CODE BEGIN Includes */
#include <stdint.h>
#include <time.h>
#include <stdlib.h>
#include "game.h"
#include "menu.h"
#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
#define KEYBOARD_REFRESH_TIME_MENU_AUTORELOAD 	2000-1
#define KEYBOARD_REFRESH_TIME_MENU_PRESCALER 	9000-1

#define KEYBOARD_REFRESH_TIME_GAME_AUTORELOAD 	2000-1
#define KEYBOARD_REFRESH_TIME_GAME_PRESCALER 	9000-1


volatile uint32_t measureStart = 0;
volatile uint32_t measureFinish = 0;

KeyboardStatus* keyboard = NULL;
Game* game = NULL;
Menu* menu = NULL;

SemaphoreHandle_t gameSemaphore = NULL;
SemaphoreHandle_t keyboardMutex = NULL;
SemaphoreHandle_t lcdMutex = NULL;

TaskHandle_t mainTaskHandle = NULL;
TaskHandle_t menuTaskHandle = NULL;
TaskHandle_t ghostsTaskHandle = NULL;
TaskHandle_t pacmanTaskHandle = NULL;
TaskHandle_t keyboardTaskHandle = NULL;
TaskHandle_t checkStatusTaskHandle = NULL;
TaskHandle_t printInformationTaskHandle = NULL;
TaskHandle_t* genericTaskHandle = NULL;

TimerHandle_t ghostMovementAlgTimer = NULL;

const char* infoGameWon[] =
{ "GAME WON", "Press '5'", "to reset" };

const char* infoGameLost[] =
{ "GAME LOST", "Press '5'", "to reset" };

uint8_t keyboardRefreshTime = KEYBOARD_REFRESH_TIME_MENU;

void ghostMovementAlgTimer_callback(TimerHandle_t xTimer)
{
	if (CHASE_MOVEMENT == game->ghostMovementType_)
	{
		game->ghostMovementType_ = SCATTER_MOVEMENT;
		xTimerReset(ghostMovementAlgTimer, pdMS_TO_TICKS(5000));
	} else if (SCATTER_MOVEMENT == game->ghostMovementType_)
	{
		game->ghostMovementType_ = CHASE_MOVEMENT;
		xTimerReset(ghostMovementAlgTimer, pdMS_TO_TICKS(20000));
	}
	xTimerReset(ghostMovementAlgTimer, pdMS_TO_TICKS(20000));
}

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void MX_FREERTOS_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if (pdPASS == xSemaphoreTakeFromISR(keyboardMutex, NULL))
	{
		BaseType_t xHigherPriorityTaskWoken = pdFALSE;
		if (BUTTON_ROW_1_Pin == GPIO_Pin)
		{
			keyboard->button_ = keyboard->setColumn_ + 1;
		} else if (BUTTON_ROW_2_Pin == GPIO_Pin)
		{
			keyboard->button_ = keyboard->setColumn_ + 4;
		} else if (BUTTON_ROW_3_Pin == GPIO_Pin)
		{
			keyboard->button_ = keyboard->setColumn_ + 7;
		} else if (BUTTON_ROW_4_Pin == GPIO_Pin)
		{
			keyboard->button_ = keyboard->setColumn_ + 10;
		}
		xSemaphoreGiveFromISR(keyboardMutex, NULL);
		vTaskNotifyGiveFromISR(*genericTaskHandle, &xHigherPriorityTaskWoken);
		portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
	}
}

static void initialize()
{
	BSP_LCD_LayerDefaultInit(LCD_BACKGROUND_LAYER,
	LCD_FRAME_BUFFER + BUFFER_OFFSET);
	BSP_LCD_SelectLayer(LCD_BACKGROUND_LAYER);
	BSP_LCD_DisplayOn();
	BSP_LCD_Clear(LCD_COLOR_BLACK);
	BSP_LCD_LayerDefaultInit(LCD_FOREGROUND_LAYER, LCD_FRAME_BUFFER);
	BSP_LCD_SelectLayer(LCD_FOREGROUND_LAYER);
	BSP_LCD_DisplayOn();
	BSP_LCD_Clear(LCD_COLOR_BLACK);
	BSP_LCD_SelectLayer(LCD_FOREGROUND_LAYER);

	BSP_LCD_SetBackColor(LCD_COLOR_BLACK);

	const char* menu1labels[] =
	{ "Start", "Stop" };

	srand(time(NULL));
	keyboard = construct_Keyboard();
	menu = construct_Menu(2, menu1labels, keyboard);
	game = construct_Game(5, keyboard);

	HAL_GPIO_WritePin(BUTTON_COL_1_GPIO_Port, BUTTON_COL_1_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(BUTTON_COL_2_GPIO_Port, BUTTON_COL_2_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(BUTTON_COL_3_GPIO_Port, BUTTON_COL_3_Pin, GPIO_PIN_RESET);
}

static void deinitialize()
{
	destructor_Game(game);
	destructor_Keyboard(keyboard);
	keyboard = NULL;
	game = NULL;
}

uint8_t isGame = 0;

void keyboard_task(void* args)
{
	while (1)
	{
		ulTaskNotifyTake(pdTRUE, portMAX_DELAY);
		setColumn(game->keyboard_);
	}
}

void checkStatus_task(void* args)
{
	Game* const game = (Game* const ) args;
	printLifes(game);
	printScores(game);
	while (1)
	{
		ulTaskNotifyTake(pdTRUE, portMAX_DELAY);
		if (game->lives_ <= 0)
		{
			game->gameStatus_ = GAME_LOST;
			vTaskDelete(pacmanTaskHandle);
			vTaskDelete(ghostsTaskHandle);
			vTaskResume(mainTaskHandle);
		}
		if (game->score_ >= 330)
		{
			game->gameStatus_ = GAME_WON;
			vTaskDelete(pacmanTaskHandle);
			vTaskDelete(ghostsTaskHandle);
			vTaskResume(mainTaskHandle);
		}
		if (pdPASS == xSemaphoreTake(lcdMutex, portMAX_DELAY))
		{
			printLifes(game);
			printScores(game);
			xSemaphoreGive(lcdMutex);
		}
	}
}

void menu_task(void* args)
{
	Menu* const menu = (Menu* const ) args;

	BSP_LCD_SetTextColor(LCD_COLOR_GREEN);

	for (uint8_t i = 0; i < menu->labelsAmount_; i++)
	{
		menu->labels_[i].isActive_ = false;
	}
	menu->labels_[0].isActive_ = true;
	menu->selectedLabel_ = 0;

	while (1)
	{
		for (uint8_t i = 0; i < menu->labelsAmount_; i++)
		{
			if (false == menu->labels_[i].isActive_)
			{

				BSP_LCD_SetBackColor(LCD_COLOR_BLACK);
				BSP_LCD_DisplayStringAt(0, menu->labels_[i].position_,
						(uint8_t*) menu->labels_[i].text_, CENTER_MODE);
			} else
			{
				BSP_LCD_SetBackColor(LCD_COLOR_BLUE);
				BSP_LCD_DisplayStringAt(0, menu->labels_[i].position_,
						(uint8_t*) menu->labels_[i].text_, CENTER_MODE);
			}
		}
		if ((ulTaskNotifyTake(pdFALSE, portMAX_DELAY)) > 0)
		{
			if (pdPASS == xSemaphoreTake(keyboardMutex, portMAX_DELAY))
			{
				if (NOT_DEFINED != keyboard->button_)
				{
					menu->labels_[menu->selectedLabel_].isActive_ = false;
					if (TWO == keyboard->button_)
					{
						menu->selectedLabel_ = (
								menu->selectedLabel_ <= 0 ?
										menu->labelsAmount_ - 1 :
										menu->selectedLabel_ - 1);
					} else if (EIGHT == keyboard->button_)
					{
						menu->selectedLabel_ = (menu->selectedLabel_ + 1)
								% menu->labelsAmount_;
					} else if (FIVE == keyboard->button_)
					{
						menu->keyboard_->button_ = NOT_DEFINED;
						vTaskResume(mainTaskHandle);
						xSemaphoreGive(keyboardMutex);
						keyboard->button_ = NOT_DEFINED;
						vTaskDelete(NULL);
					}
					menu->labels_[menu->selectedLabel_].isActive_ = true;
					keyboard->button_ = NOT_DEFINED;
				}
				xSemaphoreGive(keyboardMutex);
			}
		}
	}
}

void pacman_task(void* args)
{
	uint32_t x, y;
	Game* const game = (Game* const ) args;
	while (1)
	{
		if (ulTaskNotifyTake(pdFALSE, portMAX_DELAY) > 0)
		{
			if (NOT_DEFINED != keyboard->button_)
			{
				x = game->pacman_->x_;
				y = game->pacman_->y_;
				switch (keyboard->button_)
				{
				case TWO:
					y += -1;
					break;
				case FOUR:
					x += -1;
					break;
				case SIX:
					x += +1;
					break;
				case EIGHT:
					y += +1;
					break;
				case NOT_DEFINED:
					while (1)
						;
					break;
				default:
					print("Keyboard button illegal\r\n");
				}
				keyboard->button_ = NOT_DEFINED;
				if (!isWall(game->board_, x, y))
				{
					remove_Pacman(game->pacman_);
					moveTo_Pacman(game->pacman_, x, y);
					if (NULL
							!= game->board_->boardScores_[twoDim2OneDim(
									game->pacman_->x_, game->pacman_->y_)])
					{
						game->score_ += getScores(
								game->board_->boardScores_[twoDim2OneDim(
										game->pacman_->x_, game->pacman_->y_)]);
						remove_Score(
								game->board_->boardScores_[twoDim2OneDim(
										game->pacman_->x_, game->pacman_->y_)]);
						game->board_->boardScores_[twoDim2OneDim(
								game->pacman_->x_, game->pacman_->y_)] =
						NULL;
						xTaskNotifyGive(checkStatusTaskHandle);
					}
					draw_Pacman(game->pacman_);
					for (uint8_t i = 0; i < game->ghostsNumber_; i++)
					{
						if (isCollisionWithGhost_Pacman(game->ghosts_[i],
								game->pacman_))
						{
							game->lives_--;
							resetPosition_Ghost(game->ghosts_[i]);
							xTaskNotifyGive(checkStatusTaskHandle);
						}
					}
				}
			}
		}
	}
}

void ghost_task(void* args)
{
	Game* const game = (Game* const ) args;
	while (1)
	{
		if (ulTaskNotifyTake(pdFALSE, portMAX_DELAY) > 0)
		{
			for (int i = 0;
					i < game->ghostsNumber_ && game->lives_ > 0
							&& game->score_ < 330; i++)
			{
				remove_Ghost(game->ghosts_[i]);
				if (SCATTER_MOVEMENT == game->ghostMovementType_)
				{
					scatterMovementAlgorithm(game->ghosts_[i], game->board_);
				} else if (CHASE_MOVEMENT == game->ghostMovementType_)
				{
					chaseMovementAlgorithm(game->ghosts_[i], game->pacman_,
							game->board_);
				}
				if (isCollisionWithGhost_Pacman(game->ghosts_[i],
						game->pacman_))
				{
					game->lives_--;
					resetPosition_Ghost(game->ghosts_[i]);
					xTaskNotifyGive(checkStatusTaskHandle);
				}
				draw_Ghost(game->ghosts_[i]);
			}
			draw_Pacman(game->pacman_);
		}
	}
}

void main_task(void* args)
{
	while (1)
	{
		initialize();
		if (pdPASS == xSemaphoreTake(gameSemaphore, portMAX_DELAY))
		{
			xSemaphoreTake(keyboardMutex, portMAX_DELAY);
			genericTaskHandle = &menuTaskHandle;
			xSemaphoreGive(keyboardMutex);
			isGame = 2;
			__HAL_TIM_SET_AUTORELOAD(&htim1,
					KEYBOARD_REFRESH_TIME_MENU_AUTORELOAD);
			__HAL_TIM_SET_PRESCALER(&htim1,
					KEYBOARD_REFRESH_TIME_MENU_PRESCALER);
			__HAL_TIM_SET_COUNTER(&htim1, 0);
			xTaskCreate(menu_task, "Menu task", 256, menu, 2, &menuTaskHandle);
			vTaskSuspend(NULL);
			if (0 == menu->selectedLabel_)
			{
				initialize_Game(game);
				isGame = 1;
				__HAL_TIM_SET_AUTORELOAD(&htim1,
						KEYBOARD_REFRESH_TIME_GAME_AUTORELOAD);
				__HAL_TIM_SET_PRESCALER(&htim1,
						KEYBOARD_REFRESH_TIME_GAME_PRESCALER);
				__HAL_TIM_SET_COUNTER(&htim1, 0);
				xSemaphoreTake(keyboardMutex, portMAX_DELAY);
				genericTaskHandle = &pacmanTaskHandle;
				xSemaphoreGive(keyboardMutex);
				xTimerStart(ghostMovementAlgTimer, portMAX_DELAY);
				HAL_TIM_Base_Start_IT(&htim2);
				xTaskCreate(checkStatus_task, "Check status task", 200, game, 2,
						&checkStatusTaskHandle);
				xTaskCreate(pacman_task, "Pacman task", 800, game, 6,
						&pacmanTaskHandle);
				xTaskCreate(ghost_task, "Ghosts task", 800, game, 3,
						&ghostsTaskHandle);
				vTaskSuspend(NULL);
				HAL_TIM_Base_Stop_IT(&htim2);
				isGame = 0;
				__HAL_TIM_SET_AUTORELOAD(&htim1,
						KEYBOARD_REFRESH_TIME_MENU_AUTORELOAD);
				__HAL_TIM_SET_PRESCALER(&htim1,
						KEYBOARD_REFRESH_TIME_MENU_PRESCALER);
				__HAL_TIM_SET_COUNTER(&htim1, 0);
				xTimerStop(ghostMovementAlgTimer, 0);
				vTaskDelete(checkStatusTaskHandle);
				if (GAME_WON == game->gameStatus_)
				{
					gameInformationLines(infoGameWon, 3);
				} else if (GAME_LOST == game->gameStatus_)
				{
					gameInformationLines(infoGameLost, 3);
				}
				genericTaskHandle = &mainTaskHandle;
				do
				{
					ulTaskNotifyTake(pdFALSE, portMAX_DELAY);
				} while (keyboard->button_ != FIVE);
				keyboard->button_ = NOT_DEFINED;
			}
			xSemaphoreGive(gameSemaphore);
		}
		deinitialize();
	}
}

/* USER CODE END 0 */

/**
 * @brief  The application entry point.
 *
 * @retval None
 */
int main(void)
{
	/* USER CODE BEGIN 1 */

	/* USER CODE END 1 */

	/* MCU Configuration----------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_DMA_Init();
	MX_USART1_UART_Init();
	MX_DMA2D_Init();
	MX_FMC_Init();
	MX_I2C3_Init();
	MX_LTDC_Init();
	MX_SPI5_Init();
	MX_TIM2_Init();
	MX_TIM1_Init();
	/* USER CODE BEGIN 2 */

	CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
	DWT->CTRL &= ~0x00000001;
	DWT->CTRL |= 0x00000001;

	BSP_LCD_Init();

	gameSemaphore = xSemaphoreCreateBinary();
	keyboardMutex = xSemaphoreCreateMutex();
	lcdMutex = xSemaphoreCreateMutex();
	xSemaphoreGive(keyboardMutex);
	xSemaphoreGive(gameSemaphore);
	xSemaphoreGive(lcdMutex);

	ghostMovementAlgTimer = xTimerCreate("ghostMovementAlgTimmer",
			pdMS_TO_TICKS(20000), pdFALSE, NULL,
			ghostMovementAlgTimer_callback);

	HAL_TIM_Base_Start_IT(&htim1);
	xTaskCreate(keyboard_task, "Keyboard task", 128, NULL, 6,
			&keyboardTaskHandle);
	xTaskCreate(main_task, "Main task", 512, game, 1, &mainTaskHandle);

	vTaskStartScheduler();

	/* USER CODE END 2 */

	/* We should never get here as control is now taken by the scheduler */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while (1)
	{
		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */
	}
	/* USER CODE END 3 */

}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void)
{

	RCC_OscInitTypeDef RCC_OscInitStruct;
	RCC_ClkInitTypeDef RCC_ClkInitStruct;
	RCC_PeriphCLKInitTypeDef PeriphClkInitStruct;

	/**Configure the main internal regulator output voltage
	 */
	__HAL_RCC_PWR_CLK_ENABLE()
	;

	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue = 16;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
	RCC_OscInitStruct.PLL.PLLM = 8;
	RCC_OscInitStruct.PLL.PLLN = 180;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
	RCC_OscInitStruct.PLL.PLLQ = 4;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

	/**Activate the Over-Drive mode
	 */
	if (HAL_PWREx_EnableOverDrive() != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
			| RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

	PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_LTDC;
	PeriphClkInitStruct.PLLSAI.PLLSAIN = 216;
	PeriphClkInitStruct.PLLSAI.PLLSAIR = 2;
	PeriphClkInitStruct.PLLSAIDivR = RCC_PLLSAIDIVR_2;
	if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

	/**Configure the Systick interrupt time
	 */
	HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq() / 1000);

	/**Configure the Systick
	 */
	HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

	/* SysTick_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
 * @brief  Period elapsed callback in non blocking mode
 * @note   This function is called  when TIM7 interrupt took place, inside
 * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
 * a global variable "uwTick" used as application time base.
 * @param  htim : TIM handle
 * @retval None
 */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	/* USER CODE BEGIN Callback 0 */
////
	/* USER CODE END Callback 0 */
	if (htim->Instance == TIM7)
	{
		HAL_IncTick();
	}
	/* USER CODE BEGIN Callback 1 */
////
	else if (htim->Instance == TIM1)
	{
		BaseType_t xHigherPriorityTaskWoken = pdFALSE;
		vTaskNotifyGiveFromISR(keyboardTaskHandle, &xHigherPriorityTaskWoken);
		portYIELD_FROM_ISR(xHigherPriorityTaskWoken)
	} else if (htim->Instance == TIM2)
	{
		if (NULL != ghostsTaskHandle)
		{
			BaseType_t xHigherPriorityTaskWoken = pdFALSE;
			vTaskNotifyGiveFromISR(ghostsTaskHandle, &xHigherPriorityTaskWoken);
			portYIELD_FROM_ISR(xHigherPriorityTaskWoken)
		}
	}
	/* USER CODE END Callback 1 */
}

/**
 * @brief  This function is executed in case of error occurrence.
 * @param  file: The file name as string.
 * @param  line: The line in file as a number.
 * @retval None
 */
void _Error_Handler(char *file, int line)
{
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	while (1)
	{
	}
	/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{
	/* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
	 tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	/* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
 * @}
 */

/**
 * @}
 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
