#include <keyboard.h>

#include <common.h>
#include <main.h>
#include <stdlib.h>

uint32_t columnPins[] =
{
BUTTON_COL_1_Pin,
BUTTON_COL_2_Pin,
BUTTON_COL_3_Pin };

GPIO_TypeDef * columnPorts[] =
{
BUTTON_COL_1_GPIO_Port,
BUTTON_COL_2_GPIO_Port,
BUTTON_COL_3_GPIO_Port};


uint32_t rowPorts[] =
{
BUTTON_ROW_1_Pin,
BUTTON_ROW_2_Pin,
BUTTON_ROW_3_Pin,
BUTTON_ROW_4_Pin };

KeyboardStatus* construct_Keyboard()
{
	return (KeyboardStatus*) malloc(sizeof(KeyboardStatus));
}

void destructor_Keyboard(KeyboardStatus* const keyboard)
{
	if (NULL != keyboard)
	{
		free(keyboard);
	}
}
void setColumn(KeyboardStatus* const keyboard)
{
	for (uint8_t col = 0; col < 3; col++)
	{
		keyboard->setColumn_ = col;
		HAL_GPIO_WritePin(columnPorts[col], columnPins[col],
				GPIO_PIN_SET);
		__asm ("NOP");
		HAL_GPIO_WritePin(columnPorts[col], columnPins[col],
				GPIO_PIN_RESET);
		__asm ("NOP");
	}
}
