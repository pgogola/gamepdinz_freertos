#ifndef _KEYBOARD__H__
#define _KEYBOARD__H__

#include <common.h>
#include <stdio.h>
enum Button
{
	NOT_DEFINED = 0,
	ONE = 1,
	TWO = 2,
	THREE = 3,
	FOUR = 4,
	FIVE = 5,
	SIX = 6,
	SEVEN = 7,
	EIGHT = 8,
	NINE = 9,
	ASTERISK = 10,
	ZERO = 11,
	HASH = 12
};

typedef struct
{
	volatile enum Button button_;
	volatile uint8_t setColumn_;
} KeyboardStatus;

KeyboardStatus* construct_Keyboard();

void destructor_Keyboard(KeyboardStatus* const keyboard);

void setColumn(KeyboardStatus* const keyboard);


#endif //_KEYBOARD_H_
